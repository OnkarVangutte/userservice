package com.users.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.users.entity.User;

public interface UserDao extends JpaRepository<User, Integer>{
		
}
