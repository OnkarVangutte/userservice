package com.users.practiceCrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeCrud2Application {

	public static void main(String[] args) {
		SpringApplication.run(PracticeCrud2Application.class, args);
	}

}
