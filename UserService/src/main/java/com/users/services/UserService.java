package com.users.services;

import java.util.List;

import com.users.entity.User;

public interface UserService {

	public User getUser(int i);
	public User addUser(User data);
	public User deleteUser(int i);
	public User updateUser(int i,User data);
	public List<User> getAllUser();
}
