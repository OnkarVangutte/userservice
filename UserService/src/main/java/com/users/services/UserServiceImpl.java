package com.users.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.users.dao.UserDao;
import com.users.entity.User;

@Service
public class UserServiceImpl implements UserService {

	public UserDao userdao;

	@Override
	public User getUser(int id) {
		return userdao.getById(id);
	}


	@Override
	public User addUser(User data) {
		return this.userdao.save(data);
	}


	@Override
	public User deleteUser(int i) {
		return this.deleteUser(i);
	}


	@Override
	public User updateUser(int i, User data) {
		User user = this.userdao.getById(i);
		user.setName(data.getName());
		user.setPhone(data.getPhone());
		return this.userdao.getById(i);
	}


	@Override
	public List<User> getAllUser() {
		
		return this.userdao.findAll();
	}

}
