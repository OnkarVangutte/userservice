package com.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.users.entity.User;
import com.users.services.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/user")
	public User addData(@RequestBody User userData) {
		return this.userService.addUser(userData);
	}
	
	@GetMapping("/user/{userId}")
	public User getUser(@PathVariable String userId)
	{
		return this.userService.getUser(Integer.parseInt(userId));
	}
	
	@GetMapping("/user")
	public List<User> getAllUsers() {
		return this.userService.getAllUser();
	}

	@DeleteMapping("/user/{userId}")
	public User deletedData(@PathVariable String userId) {
		return this.userService.deleteUser(Integer.parseInt(userId));
	}
	
	@PutMapping("/user/{userId}{userData}")
	public User updateData(@PathVariable String userId,User userData) {
		return this.userService.updateUser(Integer.parseInt(userId), userData);
	}
}
